# Lab-10: Process Algebrae fundamentals

by [__Your Name Here__](mailto:name.surnameN@studio.unibo.it)

## Exercise 10-1: Out-Out-In-In, unordered

Please complete the following state graph of a coordinated system whose initial state is:
> `s0 = out(t₁).out(t₂).in(t̄).in(t̄).0 ∥ ∅` 

according to the __unordered__ semantics of Linda.

You can replace the following image with a link to the complete PlantUML graph:

![The partial state graph to be completed](http://www.plantuml.com/plantuml/svg/ZPBVJl8m6CRFUnNl8Nm9P8m_4488-H0JJp0HE48E6bQneMkNjLFHUE22YGVSXWToBIRU0rTYHvakSpWFbhNls-VtF6_JdbJOLu7Ba5nIxc4Vkt12hd30rAdWQaJl2TXMeZbIM95zIwqO0QemetEPhHvYbq1V13ubFhgc3W7YUce53f5pdtgA2euIIXcXuG41_CVpvS8N0NVwWWc_qnbmX_95jmk2qHkITMB2oPsdLyJHfrQ4CN6B7X6Q_fj1gTG5QI63brORHA0AQXS-5Sk7LLWiKrvGx-lllmMxbrVzFIDf6K8b8Rpaq_F9MAzco71DEu-sUOlKkyqMoOg1sctuM6lQsN0qk1ZFlkhL12t3p8PyjyWAITlmQl0xiAhxUQ7rTdlOXliPgWTsQeQuNa_LZPV9SZpo3vUQeJMCoDo-PkxZnytc4QlwNyUAt92iPzFYUYkE42OYn5ODI3t2Ti8ZqzoCPzJDj3hlYapYMDxADZSUnrzXZt0dSDad)

To do so, you can click on the following link and edit the PlantUML code in order to produce a complete graph:
> http://www.plantuml.com/plantuml/uml/ZPBVJl8m6CRFUnNl8Nm9P8m_4488-H0JJp0HE48E6bQneMkNjLFHUE22YGVSXWToBIRU0rTYHvakSpWFbhNls-VtF6_JdbJOLu7Ba5nIxc4Vkt12hd30rAdWQaJl2TXMeZbIM95zIwqO0QemetEPhHvYbq1V13ubFhgc3W7YUce53f5pdtgA2euIIXcXuG41_CVpvS8N0NVwWWc_qnbmX_95jmk2qHkITMB2oPsdLyJHfrQ4CN6B7X6Q_fj1gTG5QI63brORHA0AQXS-5Sk7LLWiKrvGx-lllmMxbrVzFIDf6K8b8Rpaq_F9MAzco71DEu-sUOlKkyqMoOg1sctuM6lQsN0qk1ZFlkhL12t3p8PyjyWAITlmQl0xiAhxUQ7rTdlOXliPgWTsQeQuNa_LZPV9SZpo3vUQeJMCoDo-PkxZnytc4QlwNyUAt92iPzFYUYkE42OYn5ODI3t2Ti8ZqzoCPzJDj3hlYapYMDxADZSUnrzXZt0dSDad

You should see an editor similar to the similar one:

![The PlantUML web editor](./plantuml_web_editor.png)

You can edit the PlantUML code according to the [State Chart PlantUML](http://plantuml.com/state-diagram) specification, then press the `Submit` button.

If you need to write some special character in your chart, you can use the following web application to convert LaTeX formulas into Unicode strings:
> http://vikhyat.net/projects/latex_to_unicode

Once your chart is complete, you can click on the `View as SVG` link to see the final image as a SVG.
You can then copy&paste the SVG image URL into this markdown file to include the corresponding image in this document.

## Exercise 10-2: Out-Out-In-In, ordered

Please complete the following state graph of a coordinated system whose initial state is:
> `s0 = out(t₁).out(t₂).in(t̄).in(t̄).0 ∥ ∅` 

according to the __ordered__ semantics of Linda.

You can replace the following image with a link to the complete PlantUML graph:

![The partial state graph to be completed](http://www.plantuml.com/plantuml/svg/XO_1IiD048RlynHpR8MMjCTIacBLenvgJxM79JlMePlTi3khHl5WKV15V1kVeazYQeH627XOzeV__pwOMH3b9HO6mfPjgRmgy8nkLJHouQmi-8bmdBJAXIYXdqegGyYY3EUXcxvK1U7SHS_auOur8HMbLAWfv9vBOMUXHOQ36fy1yLJbsurtqUgvCyvFf-TMizsaAJh3zzIrM5fwBEj4kbvLP8nxW1U0rSaQ1uCKGmADFYGJT55wij-zzeU_QTSVikt9rzlnJt3_yLc_TmX9OnYrm1kxkbfUrsaDZRUf_x4TK0YZHZS-xhjqO_nxqmIpB8CPMHqBymq0)

To do so, you can click on the following link and edit the PlantUML code in order to produce a complete graph:
> http://www.plantuml.com/plantuml/uml/XO_1IiD048RlynHpR8MMjCTIacBLenvgJxM79JlMePlTi3khHl5WKV15V1kVeazYQeH627XOzeV__pwOMH3b9HO6mfPjgRmgy8nkLJHouQmi-8bmdBJAXIYXdqegGyYY3EUXcxvK1U7SHS_auOur8HMbLAWfv9vBOMUXHOQ36fy1yLJbsurtqUgvCyvFf-TMizsaAJh3zzIrM5fwBEj4kbvLP8nxW1U0rSaQ1uCKGmADFYGJT55wij-zzeU_QTSVikt9rzlnJt3_yLc_TmX9OnYrm1kxkbfUrsaDZRUf_x4TK0YZHZS-xhjqO_nxqmIpB8CPMHqBymq0

## Exercise 10-3: Choice, ordered

Please complete the following state graph of a coordinated system whose initial state is:
> `s0 =(out(t₁) + out(t₂)).rd(t̄₃).0 ∥ in(t̄₄).0 ∥ in(t̄₂).out(t₃).0 + in(t̄₁).out(t₄).0 ∥ ∅` 

according to the __ordered__ semantics of Linda.

You can replace the following image with a link to the complete PlantUML graph:

![The partial state graph to be completed](http://www.plantuml.com/plantuml/svg/lO_1JW8n48RlVOeviXB8mf4GGbmnUj43yOGS6ZfYGxVTj5CLZGSg9hw8R-DJx9EukDAiXgiUcapd__wPVyQEk2MVA-PMKXVSyXnEy8PxHQT6qrG-8lJJXZWbJlWbqLeXIE9ABjnXzmNF4JBZhKJBRgL0mBoWDGXq2oiBaaQpXjFjHWCyCO1ZlbWjhV5QP4OPEnnEj91wEHg1KV8E3n5N6X-Aga9HKMjKojopPyPcHtFeT4RWKbRFT-a0MiPJYygmIQ0DttL8aguLBVhObk4bwQPGlhs3r7LZ-xiHacxjgzbs53Px8NggzyfSMgVe3I3kljQpxEpoF9jKdtaa-f48JIBCsVw07loyTW1_atiNCYxj_z_ImCQeXS_L9m00)

To do so, you can click on the following link and edit the PlantUML code in order to produce a complete graph:
> http://www.plantuml.com/plantuml/uml/lO_1JW8n48RlVOeviXB8mf4GGbmnUj43yOGS6ZfYGxVTj5CLZGSg9hw8R-DJx9EukDAiXgiUcapd__wPVyQEk2MVA-PMKXVSyXnEy8PxHQT6qrG-8lJJXZWbJlWbqLeXIE9ABjnXzmNF4JBZhKJBRgL0mBoWDGXq2oiBaaQpXjFjHWCyCO1ZlbWjhV5QP4OPEnnEj91wEHg1KV8E3n5N6X-Aga9HKMjKojopPyPcHtFeT4RWKbRFT-a0MiPJYygmIQ0DttL8aguLBVhObk4bwQPGlhs3r7LZ-xiHacxjgzbs53Px8NggzyfSMgVe3I3kljQpxEpoF9jKdtaa-f48JIBCsVw07loyTW1_atiNCYxj_z_ImCQeXS_L9m00

## Exercise 10-4: Three Dining Philosophers

Please complete the following state graph of a coordinated system whose initial state is:
> `s0 = in(t̄₁).in(t̄₂).out(t₁).out(t₂).0 ∥ in(t̄₂).in(t̄₃).out(t₂).out(t₃).0 ∥ in(t̄₃).in(t̄₁).out(t₃).out(t₁).0 ∥ t₁ ∪ t₂ ∪ t₃` 

according to the __simplified ordered__ semantics of Linda.

(**One possible solution is available [here](http://www.plantuml.com/plantuml/uml/R90nJW9158RxESKhmGIoO4S8CO8cDZJ42YWcEq-SC3kpcNcheh5WryABs7WMJy59d0MURh7Tb_TzFs7sAf3qL6H6XAsskVGoWna-oCBGhREqqAy8mwGX5oG9Zufs1a6JD3eDxUkvp12chl0QlNZK2W6pd7QWCAHUvwIT5Orbg7yQtWJ0jKot6-yAgwREE3yUtrYbxMOo0MVq4xOLCvJAk7GR6u-ghRYt-997GXMR7HhZdiMx4CkBDVHw4mY9EFw122FGjaC_7uTot-qpbjs-AwX3zP4ftfAL1JXyVa6ZOwmQ8LDrdW2Fk6JSDrAc_Nd4i_eKuRk6ag4I8JczP50_uBHRsUNTFCBwJmvPbYpBVtd9ic9PhGL_bm6CuBTvQqTovOQ1-Jk5ZZ13WXzkv1iuiMzWfqxs3BwtnHAgHq5NYwQ6emrU1DHZ0mDq0r3j3yt6SnBsHoY6E8yam9X84c3CAWdXDbyTwqi2IGWU9wa46EiH9v80OrD9YBVhMcIwBtvzHs-7S6G1iS2QIOfWRAHEa0MxP-6dD85_TlE8eoFpg7x_yuyUrc4tzNNJH9ylZYivxiHA5bRPk1bHVRvGlJwpuwPK1MjMDLZdEd4T1_twhewRSG-i8Nm112hqAQvs5SsEnzKyBaOOi0Q3bIbek90u1ujm3q1EwWpck7tXVyG7GsYW787yQWdSeoFVIQdrJmQ7e7YZT-T7oUB-xzyxM0DOGks0YxRx40i0RdAuKVVvdc_Fi_iuzcMf1hGGfdfrHKLxguRWIJNIYxrPuH0PHo4mMyojOoKIpC4cpIpnmdriRCE86v0poeGvarP5O0zY-b7hOzmTUA5nmLpbRX26XM21ICS0Z9pquT24KAE1hcvutrjSc6oZWo7CEwS1137Nvmlhom2SZL0m1rbVQJ5xoKKa9iSOwIO2ixre_NmTfurzckp0-TGEiwbbfFF6nouUiJZ4JByCp4KLLwUDpvqhAY0o-eMvIkPgy_Gxf-En0HUdN3WqBVITxYvXMa0tL-gqyQ7wSD1K2qPRnAzw7B4T89bSm2nfrUQctNCgtm8WClQ1EQhAjxZY52BB9pekuqlEuP2q7DZgYLmHfvc3v8SDcEdj3H6anTSrsG398HRQ031N9LvTDowvZrbsE4slnZQdfXE0N1hp9jiBqhZu86KLS9WCmho_K9DpXL3St1i0SVs3Gz0ia3QA-FmAJ1kjG226GuvQzY2BgxGEz_yTWBXGmq7_iX4cP7hcify2u1160fpuhkQEmeaE9-cUINRUoKi3CWu8oWc7_A9dqWhHPpsJ752XAWbprgIUyeeJ9-kO3Bn2jbDPSDocS8hWmEUMg5VbfuxafpFa5PVTr1nDrOJRDN7PGJPl3aBkQG-Xk-BCcdVW8OC0p6LJ586IMtPB0Tm0F5HLWITIFASVH79qF3l7SpZ8nR1BsEb2YP8pG92qaETe_l-NN78yyKLDvVE4Kl52pex7gEh0hESqaDofqeXlM81HIb_djWkEpBawd2Ih6iH-tcquDt-PNo1XBhgsEdzSSF4j6-28hRZibtoqiezyjCAP_AtbjmIyp5aI3lBiMjuUvSVo51rC4p_mWYrEeIHdek9CNUblKZ2Nffcef_daus59ZcnN7wWtvHatlvBzjS3Pxd2G4lBekvClUQNWQJ7JbggwQ7nSB1Lvvvq31zFpP6bJMxdIyTBywQJsxph-WuDKmBhvQvbFCOT3qjtHK_d6cbztVwReXFVQVMDvhu4_WnQ0GzKjqEjqUYMdpz31enej-55PhuPmUPt3eRq8DurFqWn74iyyLtMcuGvdDIEhF5muIAf_v6j-4g_EPKrkTm-Oy_DrhNX5nz9q2MbUhWsloCsmRRUdFtNnk2R6XD_-_kUl-v_q_0S0)**)

You can replace the following image with a link to the complete PlantUML graph:

![The partial state graph to be completed](http://www.plantuml.com/plantuml/svg/R8_1JW8n443l_OevWWbazKW8CO8cNk20di13inrnGhVTjBCg6W-21t_4Yz_YL_0bTgrDUjZRo_IzJkRSitJSL5huBPbQEbd13Ezbe_dA6bxI2y9PaJqkMJB-FV5E8n1BJQNlhkUoGfZQnX4wyK0A0QkQTw3Gbuvg9cj4LlhsQtWL01Uot6wSROoQMctTU7nf59dDP09MqoDs6JEKIjjo6no7gikuDVXS1q8Ld1rgRP_4cn1nQyeX_sa4DulP0enbAjjJXLYTtC5WC05Vn2x02CZq-EVZk7_l3nFk-qFRS8_3C54xAzO6uFZv1PcNy929YGx_IHP5CrkPr6nZYcBMZW9yjx1FS12-Y9USjWV4RcrMr_C0lyrNlMw6C0pSUOeymeVp8vAfRz2b7TdQjGjl0Efx5mbVJS6AcbQ134mdeRlpdJ7ZDqW0Pu2pW1RW0d0PE_ZIcsHQTEDWGhz9SQ9JJ03eDHC0Xfm9PFwHZlgHW0a4dKeI00uddRE0CMU2DPTNfBmNtMIv3YGkY1QWGRC8ODHo0XVSwa8h9U2nErPicHchUdvzsMG1Txdf-lAUrzRbcRTN8miqMzm0LGq4bVCPcBU-ge26N0Q7lpMntWxgfW6pqee5b2W92IHqfolMNhFbkYBceUa2IavGvGjA5oxENuQm9p1NSYQmVIHv8mQsbsRDptiicOQEf4LuEftdNQjWVJw5d3-HkwlH2hbmkAHlAgL2ZKZnhRXbrIvquNaCN-KbLsIYVUJ9rLzL-SF-wVtZRVsh_G80)

To do so, you can click on the following link and edit the PlantUML code in order to produce a complete graph:
> http://www.plantuml.com/plantuml/uml/R8_1JW8n443l_OevWWbazKW8CO8cNk20di13inrnGhVTjBCg6W-21t_4Yz_YL_0bTgrDUjZRo_IzJkRSitJSL5huBPbQEbd13Ezbe_dA6bxI2y9PaJqkMJB-FV5E8n1BJQNlhkUoGfZQnX4wyK0A0QkQTw3Gbuvg9cj4LlhsQtWL01Uot6wSROoQMctTU7nf59dDP09MqoDs6JEKIjjo6no7gikuDVXS1q8Ld1rgRP_4cn1nQyeX_sa4DulP0enbAjjJXLYTtC5WC05Vn2x02CZq-EVZk7_l3nFk-qFRS8_3C54xAzO6uFZv1PcNy929YGx_IHP5CrkPr6nZYcBMZW9yjx1FS12-Y9USjWV4RcrMr_C0lyrNlMw6C0pSUOeymeVp8vAfRz2b7TdQjGjl0Efx5mbVJS6AcbQ134mdeRlpdJ7ZDqW0Pu2pW1RW0d0PE_ZIcsHQTEDWGhz9SQ9JJ03eDHC0Xfm9PFwHZlgHW0a4dKeI00uddRE0CMU2DPTNfBmNtMIv3YGkY1QWGRC8ODHo0XVSwa8h9U2nErPicHchUdvzsMG1Txdf-lAUrzRbcRTN8miqMzm0LGq4bVCPcBU-ge26N0Q7lpMntWxgfW6pqee5b2W92IHqfolMNhFbkYBceUa2IavGvGjA5oxENuQm9p1NSYQmVIHv8mQsbsRDptiicOQEf4LuEftdNQjWVJw5d3-HkwlH2hbmkAHlAgL2ZKZnhRXbrIvquNaCN-KbLsIYVUJ9rLzL-SF-wVtZRVsh_G80

## Exercise 10-5: Writing transition rules

Try extending the CS definition with new transition rules formally specifying the semantics of the `no`, `inp`, `rdp`, and `nop` primitives

For instance, the following rules are for `nop`:

<img src="https://tex.s2cms.ru/svg/%0A%5Cfrac%7B%0A%09%5Cforall%20t%20%5Cin%20%5Cbar%7Bt%7D%20%3A%20TS%20%5Cneq%20TS'%20%5Ccup%20t%0A%7D%7B%0A%09US%20%5Cparallel%20%5Cmathtt%7Bnop%7D(%5Cbar%20t)%5C%20%3F%5C%20U%20%3A%20U'%20%5Cparallel%20US'%20%5Cparallel%20TS%0A%09%5Cxrightarrow%7Bnop(%5Cbar%20t%2C%20%5Ctop)%7D_%5Cmathcal%7BCS%7D%0A%09US%20%5Cparallel%20U%20%5Cparallel%20US'%20%5Cparallel%20TS%0A%7D%20%0A%5Cqquad%0A%5B%5Ctext%7BNOP-T%7D_i%5D%0A" alt="
\frac{
	\forall t \in \bar{t} : TS \neq TS' \cup t
}{
	US \parallel \mathtt{nop}(\bar t)\ ?\ U : U' \parallel US' \parallel TS
	\xrightarrow{nop(\bar t, \top)}_\mathcal{CS}
	US \parallel U \parallel US' \parallel TS
} 
\qquad
[\text{NOP-T}_i]
" />

<img src="https://tex.s2cms.ru/svg/%0A%5Cfrac%7B%0A%09%5Cexists%20t%20%5Cin%20%5Cbar%7Bt%7D%20%3A%20TS%20%3D%20TS'%20%5Ccup%20t%20%5Cquad%20%0A%7D%7B%0A%09US%20%5Cparallel%20%5Cmathtt%7Bnop%7D(%5Cbar%20t)%5C%20%3F%5C%20U%20%3A%20U'%20%5Cparallel%20US'%20%5Cparallel%20TS%20%0A%09%5Cxrightarrow%7Bnop(%5Cbar%20t%2C%20%5Cbot)%7D_%5Cmathcal%7BCS%7D%0A%09US%20%5Cparallel%20U'%20%5Cparallel%20US'%20%5Cparallel%20TS%0A%7D%20%0A%5Cqquad%0A%5B%5Ctext%7BNOP-F%7D_i%5D%0A" alt="
\frac{
	\exists t \in \bar{t} : TS = TS' \cup t \quad 
}{
	US \parallel \mathtt{nop}(\bar t)\ ?\ U : U' \parallel US' \parallel TS 
	\xrightarrow{nop(\bar t, \bot)}_\mathcal{CS}
	US \parallel U' \parallel US' \parallel TS
} 
\qquad
[\text{NOP-F}_i]
" />

You can use [upmath.me](https://upmath.me/) to produce markdown files including formulas.

## Exercise 10-6: Coffee Machine

You must perform and end-to-end formalisation of a C/DS composed by a coffee machine and the user interacting with it.

The system must take into account the following requirements:

- Any coffee machine simply performs the following sort of actions:
    * it initially `waits` for `coins` to be inserted
    * it then `checks` if coins are sufficient
    * it may optionally give `change` back to the user
    * it serves the `coffee` to the user
    * it finally `waits` for the `user` to take the coffee 

- In turn, any user can perform the following actions:
    * he/she can `walk` around
    * he/she can `chat` with some friends
    * he/she can insert coins into the coffee machine in order to `pay`
    * it can `take` the coffee the machine has eventually served

- Of course, coffee machines can stop waiting for money only if some user pays 
Similarly, they can stop waiting for the coffee to be taken only if some user takes it


Your formalisation must provide an interpretation and a semantics for the following formula:
> `s0 = (chat + walk).insert.(walk + chat).take.0 ∥ waitCoin.check.(change.coffee + coffee).waitUser.0`

You must finally draw the state graph of the system having `s0` as initial state, according to your semantics
You can replace the following image with a link to the complete PlantUML graph:

![The partial state graph to be completed](http://www.plantuml.com/plantuml/svg/ROx1JW8n48RlVOevWePcuz4GGWGzUY6SZ8T9Ey1aPpkRTbP4uzqNySLy4hUSCE7sjz_tDLqMmsn39wwqd7hCsC6QTZY8FMco3NyGt3KNS6DeT4Ps4W8s50xbUlsC7S5AXyoKtO4Z0NMzdI1I2PbxOqtkeYprQVXq00yOsdtM8SMLYkRv_355Jll50bJuZQuPQy8e6jhHoHIl6Tj4xxteI4VdH29wT5_ElToymcosWDAuymV60V_bUvY40nhSmX6bdNfEXRBvIJsDbvLDlM5Ble7Vxv_HObif9nyE55fVuxGd7tIt8wh1UKnzzRQ5ic_Sab8SEla3)

To do so, you can click on the following link and edit the PlantUML code in order to produce a complete graph:
> http://www.plantuml.com/plantuml/uml/ROx1JW8n48RlVOevWePcuz4GGWGzUY6SZ8T9Ey1aPpkRTbP4uzqNySLy4hUSCE7sjz_tDLqMmsn39wwqd7hCsC6QTZY8FMco3NyGt3KNS6DeT4Ps4W8s50xbUlsC7S5AXyoKtO4Z0NMzdI1I2PbxOqtkeYprQVXq00yOsdtM8SMLYkRv_355Jll50bJuZQuPQy8e6jhHoHIl6Tj4xxteI4VdH29wT5_ElToymcosWDAuymV60V_bUvY40nhSmX6bdNfEXRBvIJsDbvLDlM5Ble7Vxv_HObif9nyE55fVuxGd7tIt8wh1UKnzzRQ5ic_Sab8SEla3
